using System;
using Xunit;

namespace dockergitlab.Test
{
    public class VoidTests
    {
        [Fact]
        public void Void_IfStringEqualsString_ShouldBeTrue()
        {
            Assert.Equal("equal", "equal");
        }
    }
}
